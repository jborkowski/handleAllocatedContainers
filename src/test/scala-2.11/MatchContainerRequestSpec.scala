import org.scalatest.{FlatSpec, Matchers}

class MatchContainerRequestSpec extends FlatSpec with Matchers {

  "handleAllocatedContainers" should "split input collection to 4 collections" in {
    case object Hello
    case object LeaveMeAlone

    val predSeq = Seq(???)
    val input = Seq("hello",1, 2, 3, true, LeaveMeAlone, true, Hello, Hello)
    val actual = YarnAllocator.handleAllocatedContainers(input)
    val bucketInts = 3 :: Nil
    val bucketBoolean = true :: false :: Nil
    val bucketHomeLeaveMeAlone = Seq(Hello, Hello, LeaveMeAlone)
    val remainingAfterOffRackMatches = Seq("hello", 1)
    val expected = bucketInts :: bucketBoolean :: bucketHomeLeaveMeAlone :: remainingAfterOffRackMatches :: Nil



    actual should be (expected)
  }
}
