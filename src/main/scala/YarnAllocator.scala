object YarnAllocator {
  type Containers = Seq[Any]
  final case class Result(matched: Containers, remaining: Containers)
  def handleAllocatedContainers(allocatedContainer: Seq[Any])(f: Seq[Containers => Result]): Seq[Seq[Any]] = {

    case class MatchAcc(remaining: Containers, matched: Seq[Containers])

    val MatchAcc(remaining, matched) = f.foldLeft(MatchAcc(allocatedContainer, Seq[Containers]())){
      case (MatchAcc(remaining, matched), f) => {
        val matchResult = f(remaining)
        MatchAcc(matchResult.remaining, matched :+ matchResult.matched)
      }
    }
    matched :+ remaining
  }
}